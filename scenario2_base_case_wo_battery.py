'''
See scenario2.py
'''
from scenario2 import *

def unsched_total_inelastic():
  ''' This is the total consumption of all load devices after network initialization (which solves
  as p=0. Therefor it is the consumption when sites have no DR / don't care about price. Need this
  to generate optimal battery/pv/gen profiles for base case comparison.

  Generated this by loading this file: n = make_network(); n.init(); n.demand
  '''
  profile = array([ 6.49658048,  5.89721676,  5.89733771,  5.89695838,  6.12668084,
        4.99055124,  5.31043338,  5.61929957,  7.2865039 ,  5.40050116,
        5.04979827,  5.24983109,  8.53073663,  8.54985521,  8.39893515,
        8.68044215,  8.63039405,  9.87872299, 11.73584197, 12.8871738 ,
        7.61136775,  6.76883262,  8.2089477 ,  7.04739561])
  return {
    'id': 'load',
    '_type': 'Device',
    'on': (0, 23),
    'bounds': profile,
  }


def make_deviceset():
  return DeviceSet('mg', make_devices())


def make_devices():
  return [
    make_device(gen()),
    make_device(pv()),
    make_device(unsched_total_inelastic())
  ]
