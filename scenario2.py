'''
This is a spin off of scenario 1:

There 8 rural mixed living and working type sites connected to a microgrid. The microgrid has central:

  - Gas micro turbine (MT) generator.
  - PV installation.
  - Large chemical battery.

There are ~three types of sites.

  - An unschedulable load component which is randomly generated within some bounds.
  - The same make up base schedulable or scalable devices.
    - Electrical Vehicle.
    - Avg to large sized domsestic fridge.
    - A water pump.
    - Lighting. Lights can be dimmed.
  - 1/2 of the sites run a water pump that can operate at any time of day [7,19]. The other 1/2 of the pumps must only in the afternoon [12-19].
  - Two of the site types run specialized equipment:
    - Farmer type runs a large machine during the day time.
    - Hunter type runs a large freezer/refrigerator 24/7 in addition to base refrigeration requirements.
  - Additionally, sites are either early, mid or late, which determines when lights are on and when EV can charge. Farmers are early Hunters are late, Normal are a mix.

In this scenario there are (2,2,4) (Farmer, Hunter, Normal) sites.

The following device models are required for sites:

  - Unschedulable; All loads a folded into one unschedulable device load.
  - EV;
  - Thermal devices; Fridge and freezer are modeled as such.
  - Interuptable shiftable; Pump is modeled as such.
  - Uninteruptable shiftable; Machinary is modeled as such.

The later three types have discrete run levels constraints. The last type also has a discrete start time constraint.

**Scenario:**

    Base:
      Pump: CDevice*
      EV: CDevice
      Fridge: TDevice*
      Lighting: IDevice
    Farmer extends Base:
      Machinary: BlobDevice*
    Hunter extends Base:
      Freezer: TDevice*
    Gen: GDevice*
    PV: PVDevice
    Battery: SDevice

  - "*" Indicates the device has additional non convex or discrete contraints. Almost everything does. These are ignore in the convex relaxed solution and remapped. This is the main point of the experiment.
  - Initially the EV will be modeled as CDevice but want to add Max_RoC(SoC) constraint.

**Approximate sizing**

  - Standard Large Fridge: 2880Wh; [0, 120, 240]W
  - Large Fridge/Freezer: 6000Wh; [0, 250, 500]W
  - Machinary; 8000Wh; [0, 500, 1000, 1500, 2000]W
  - EV: 8000Wh; 0-2000W
  - Water Pump; 6000Wh; [0, 500, 1000, 1500]W
  - Lighting: XWh; 100-200W

**Base device specification:**

  _type: powermarket.device_kit type
  id: relative id of this device.
  bounds: power bounds
  cbounds: 2-tuple cummulative bounds.
  on: times when device can run.
  levels: run levels as small integers. Actual consumption requires multiplying by step.
  step: see levels.

If `bounds` is a 2-tuple then `on` must be set. If bounds is not a 2-tuple it must be in the style
required by powermarket.Device and `on` cannot be set.

If `step` field is present `bounds`, `cbounds`, `level` must all be in lowest common multiple form.
The actual values are obtained by multiplying by `step`. Done this way to make enumerating feasible
discrete profiles easier.
'''
import sys
import logging
from logging import debug, info
from copy import deepcopy
from pprint import pprint
import numpy as np
from numpy import array, zeros, ones, hstack, concatenate
from powermarket.network import *
from powermarket.device_kit import *
from powermarket.network.lcl_network import LCLNetwork


meta = {
  'title': ''
}


def light(on=(0, 23)):
  return {
    'id': 'light',
    '_type': 'IDevice2',
    'on': on,
    'bounds': [0.05, 0.2],
    'd_0': 0.04
  }


def ev(on):
  return {
    'id': 'ev',
    '_type': 'CDevice2',
    'on': on,
    'bounds': [0, 2],
    'cbounds': [4.5, 6],
    'd_0': 0.04
  }


def unsched(i=0):
  profiles = np.array([
    [0, 0, 0, 0, 0, 0, 550, 300, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 250, 300, 300, 10, 100, 0],
    [0, 0, 0, 0, 0, 250, 250, 350, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 300, 300, 400, 100, 0, 0],
    [0, 0, 0, 0, 0, 0, 250, 550, 150, 100, 100, 200, 0, 0, 0, 0, 100, 150, 100, 200, 300, 100, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 550, 300, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 250, 300, 300, 10, 100],
    [0, 0, 0, 0, 0, 0, 0, 250, 250, 350, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 300, 300, 400, 100],
    [20, 20, 20, 20, 250, 350, 20, 20, 20, 20, 20, 20, 200, 20, 20, 550, 300, 200, 20, 20, 20, 20, 20, 20],
    [0, 0, 0, 0, 0, 0, 0, 300, 0, 0, 0, 0, 100, 400, 250, 0, 0, 250, 300, 350, 100, 0, 0, 0],
    [50, 50, 50, 50, 50, 200, 50, 200, 0, 0, 0, 100, 100, 0, 0, 0, 100, 550, 0, 100, 100, 150, 250, 0],
  ])/1000
  return {
    'id': 'unsched',
    '_type': 'Device',
    'on': (0, 23),
    'bounds': profiles[i],
  }


def pump(on=(12, 19), step=0.4):
  return {
    'id': 'wpump',
    '_type': 'Device',
    'on': on,
    'bounds': [0, 3],
    'cbounds': [16, 16],
    'levels': [0, 3],
    'step': step
  }


def fridge(step=0.12):
  ''' The fridge has cbounds for two reasons. 1. reduces the numberof combinations. 2. Is a weak
  mech to help ensure temperature doesn't stay too high consistently.
  '''
  return {
    'id': 'fridge',
    '_type': 'TDevice',
    'bounds': [0, 2],
    'cbounds': [12, 36],
    't_external': ones(24)*24,
    't_init': 3,
    't_optimal': 3,
    't_range': 4,
    'sustainment': 1-0.07429, # Loss factor
    'efficiency': -13, # Efficiency factor * unit conversion (kW to Celcius)
    'on': (0, 23),
    'levels': [0, 2],
    'step': step
  }


def freezer(step=0.25):
  ''' @see fridge(). '''
  return {
    'id': 'freezer',
    '_type': 'TDevice',
    'bounds': [0, 2],
    'cbounds': [12, 36],
    't_external': ones(24)*24,
    't_init': -18,
    't_optimal': -18,
    't_range': 4,
    'sustainment': 1-0.07143, # Loss factor
    'efficiency': -12, # Efficiency factor * unit conversion (kW to Celcius)
    'on': (0, 23),
    'levels': [0, 2],
    'step': step
  }


def machine(on=(8, 19), step=1):
  ''' Used to be a Blob or Window device, now just a device. '''
  return {
    'id': 'machine',
    '_type': 'Device',
    'on': on,
    'bounds': [0, 2],
    'cbounds': [8, 8],
    'levels': [0, 2],
    'step': step,
    'run_time': [4,5] # Min/max runtime.
  }


def pv():
  pv_prof = -5*array([ 10, 10, 10, 11, 37, 202, 458, 729, 972, 1124, 1190, 1179, 1133, 1081, 910, 802, 621, 461, 276, 122, 112, 92, 16, 9])
  return {
    'id': 'pv',
    '_type': 'PVDevice',
    'bounds': (pv_prof/1000, 0),
  }


def gen():
  '''
  Some cost function options:
    - RM: array([0.05, 1, 0])*1e-6
    - Li Na: [0.00045, 0.0058, 0.024, 0]
    - Zhang#1: [0.006/1e6, 0.50/1e3]
    - Zhang#2: [0.003/1e6, 0.25/1e3]
  '''
  return {
    'id': 'supply',
    '_type': 'GDevice',
    'bounds': [-11.25, -2],
    'cost': [0.028, 0.30, 0.],
    'run_time': [4,None] # Minimum runtime. No max runtime. Assume can run for at least a day.
  }


def battery():
  return {
    'id': 'battery',
    '_type': 'SDevice',
    'bounds': [-9.5, 9.5],
    'c1': 0.0,
    'c2': 0.0,
    'c3': 0.0,
    'reserve': 0.5,
    'capacity': 32,
    'efficiency': 0.97
  }


'''
Let's have early, mid, late risers. Seems reasonable farmers are early risers. Hunters stay up late.
Note rel on times: They re inclusive, and "0" -> 00:00-01:00, "23" -> 23:00-00:00
'''
sites = {
  'site0': [
    unsched(0),
    fridge(),
    light((17, 22)), # Early
    ev((0, 4, 18, 23)), # Early
    pump((12, 19)),
  ],
  'site1': [
    unsched(1),
    fridge(),
    light((18, 23)), # Mid
    ev((0, 6, 20, 23)), # Mid
    pump((8, 19)),
  ],
  'site2': [
    unsched(2),
    fridge(),
    light((0, 0, 19, 23)), # Late
    ev((0, 8, 22, 23)), # Late
    pump((12, 19)),
  ],
  'site3': [
    unsched(3),
    fridge(),
    light((18, 23)), # Mid
    ev((0, 6, 20, 23)), # Mid
    pump((8, 19)),
  ],
  'site4': [
    unsched(4),
    fridge(),
    light((17, 22)), # Early
    ev((0, 4, 18, 23)), # Early
    pump((12, 19)),
    machine(),
  ],
  'site5': [
    unsched(5),
    fridge(),
    light((17, 22)), # Early
    ev((0, 4, 18, 23)), # Early
    pump((8, 19)),
    machine(),
  ],
  'site6': [
    unsched(6),
    fridge(),
    light((0, 0, 19, 23)), # Late
    ev((0, 8, 22, 23)), # Late
    pump((12, 19)),
    freezer(),
  ],
  'site7': [
    unsched(7),
    fridge(),
    light((0, 0, 19, 23)), # Late
    ev((0, 8, 22, 23)), # Late
    pump((8, 19)),
    freezer(),
  ],
}


def make_deviceset():
  return DeviceSet('mg', make_devices())


def make_devices():
  devices = [
    make_device(gen()),
    make_device(pv()),
    make_device(battery())
  ]
  devices += make_site_devices(sites)
  return devices


def make_site_devices(sites):
  devices = []
  for site_id, site_spec in sites.items():
    devices.append(make_site_device(site_id, site_spec))
  return devices


def make_site_device(site_id, site_spec):
  devices = []
  debug(site_spec)
  for device_spec in site_spec:
    devices.append(make_device(device_spec))
  return DeviceSet(site_id, devices)


def make_device(device_spec):
  device_spec = deepcopy(device_spec)
  device_type = getattr(sys.modules[__name__], device_spec['_type'])
  del device_spec['_type']
  device_spec = prep_device_spec(device_spec)
  device_spec['length'] = 24
  device = device_type(**device_spec)
  return device


def prep_device_spec(device_spec):
  ''' Convert pseudo device spec of the scenario into compat with device_kit.
  @todo on2bounds, maybe even step handled in device_kit.
  '''
  device_spec = on2bounds(device_spec, 24)
  device_spec = stepup(device_spec)
  return device_spec


def on2bounds(device_spec, l):
  ''' Convert on, and bounds pair to powermarket style bounds '''
  if 'on' not in device_spec.keys():
    return device_spec
  device_spec = deepcopy(device_spec)
  on = device_spec['on']
  bounds = device_spec['bounds']
  on_vector = np.zeros(l)
  for i in range(0, len(on), 2):
    on_vector[on[i]:on[i+1]+1] = 1
  if len(bounds) == 2:
    device_spec['bounds'] = (on_vector*bounds[0], on_vector*bounds[1])
  else:  # Assume bounds is a vector
    device_spec['bounds'] = (on_vector*bounds, on_vector*bounds)
  return device_spec


def stepup(device_spec):
  if 'step' not in device_spec.keys():
    return device_spec
  device_spec = deepcopy(device_spec)
  device_spec['cbounds'] = np.array(device_spec['cbounds'])*device_spec['step']
  device_spec['bounds'] = np.array(device_spec['bounds'])*device_spec['step']
  return device_spec


if __name__ == '__main__':
  pass
