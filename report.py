#!/usr/bin/env python3
''' Generate all images and tables used in reporting based off latest simulation result in this dir. '''
import sys
import re
import numpy as np
import pandas as pd
import csv
import matplotlib.pyplot as plt
from powermarket.network import *
from powermarket.device_kit import *
from powermarket.writer import *
from powermarket.analytics.templates import *
from powermarket.analytics.matplotlibwriter import *
from copy import deepcopy


np_printoptions = {
  'linewidth': 1e6,
  'threshold': 1e6,
  'formatter': {
    'float_kind': lambda v: '%+0.3f' % (v,),
    'int_kind': lambda v: '%+0.3f' % (v,),
  },
}
pd.set_option('display.float_format', lambda v: '%+0.4f' % (v,),)
pd.set_option('display.width', 1000)

d = 'scenario2.py-network-latest'
d_base = 'scenario2.py-network-latest-base-case'
reader = NetworkReader(d)
(first, last) = (reader.first(), reader.last())
reader = NetworkReader(d_base)
(first_base, last_base) = (reader.first(), reader.last())

n0 = pd.read_csv('%s-report/network-0.csv' % (d,), header=None, index_col=0)
n1 = pd.read_csv('%s-report/network-1.csv' % (d,), header=None, index_col=0)
n1_base = pd.read_csv('%s-report/network-1.csv' % (d_base,), header=None, index_col=0)
n0_load = n0.loc[filter(lambda x: x.find('site') > 0, n0.index)]
n1_load = n1.loc[filter(lambda x: x.find('site') > 0, n1.index)]
n1_der = n1.loc[filter(lambda x: not x.find('site') > 0, n1.index)]
n1_discrete = n1.loc[filter(lambda x: re.match('.*(wpump|fridge|freezer|machine)$', x), n1.index)]
n1_base_load = n1_base.loc[filter(lambda x: x.find('load') > 0, n1_base.index)]
nm = pd.read_csv('%s-report/network-1.mapped.csv' % (d,), header=None, index_col=0)
nm_load = nm.loc[filter(lambda x: x.find('site') > 0, nm.index)]
last_nm = deepcopy(last)

if not os.path.isdir('scenario2.py-network-fitting-report'):
  os.mkdir('scenario2.py-network-fitting-report')

def main():
  print('### TABLE 1')
  table_summary_stats()
  print('### TABLE 2')
  table_load_by_type()
  print('### TABLE 3')
  table_fitting_error()
  plot_gen_profile()
  plot_pv_profile()
  plot_unsched_profile()
  plot_base_case_profile()
  plot_aggregate_fitting()
  plot_fittings()
  plot_thermals()


def print_summary_stats(network, table):
  suppliers = [a for a in network.agents if a.id.find('supply') >= 0]
  zeros = np.zeros(len(network))
  _str = ''
  _str += '%-22s %d\n' % ('num_agents', len(network.agents))
  _str += '%-22s %.3f\n' % ('load_factor', network.lf)
  _str += '%-22s %.3f\n' % ('peak', network.demand.max())
  _str += '%-22s %.3f; %.3f\n' % ('excess (tot/avg)', network.r.sum(), np.average(network.r))
  _str += '%-22s %.3f; %.3f\n' % ('demand (tot/avg)', table.sum().sum(), np.average(table.sum()))
  _str += '%-22s %s\n' % ('supply cost (tot)', [round(a.u(p=0), 4) for a in suppliers] if suppliers else 'NO SUPPLIER FOUND')
  _str += '%-22s %s\n' % ('supply prices (avg)', [round(np.average(a.deriv(p=0)), 4) for a in suppliers] if suppliers else 'NO SUPPLIER FOUND')
  _str += '%-22s %s\n' % ('supply prices', [np.round(a.deriv(p=0), 4) for a in suppliers] if suppliers else 'NO SUPPLIER FOUND')
  print('-'*10)
  print(_str)


def table_summary_stats():
  ''' Battery is included as a load because 1. it has an efficiency factor which is a load and
  2. because it is a load half the time anyway.
  Note the initial solution in the "first" network is not valid because supply does not match demand.
  To calculate the actual costs we need to account for both supply (generation) and demand (disutility
  of demand response). The it is done here is a bit messy but works.
  '''
  n1_base_load = n1_base.loc[filter(lambda x: re.match('mg\.load', x), n1_base.index)]
  n1_load = n1.loc[filter(lambda x: re.match('mg\.site.*', x), n1.index)]
  nmn_load = nm.loc[filter(lambda x: re.match('mg\.site.*', x), nm.index)]
  supply = last.find_agents('supply')[0] # We just need the cost (utility) function which is renetrant.
  last_sites = [v for k,v in dict(last.leaf_agents()).items() if re.match('mg\.site.*', k)]
  first_sites = [v for k,v in dict(first.leaf_agents()).items() if re.match('mg\.site.*', k)]
  delta_u = sum([a.u() for a in last_sites]) - sum([b.u() for b in first_sites])
  balance = (nmn_load - n1_load).sum()
  index_pres = [
    'total load (kW)',
    'peak load (kW)',
    'load factor',
    'generation cost (\\$)',
    'disutility cost (\\$)',
    'total cost (\\$)'
  ]
  index = [
    'total_load',
    'peak_load',
    'load_factor',
    'generation_cost',
    'disutility_cost',
    'total_cost'
  ]
  df = pd.DataFrame({}, index=index)
  df['NODR'] = [
    n1_base_load.sum().sum(),
    n1_base_load.sum().max(),
    np.maximum(n1_base_load, 0).sum().mean()/np.maximum(n1_base_load, 0).sum().max(),
    -supply.u(n1_base.loc['mg.supply'].values, p=0),
    0,
    -supply.u(n1_base.loc['mg.supply'].values, p=0) - 0,
  ]
  df['DR'] = [
    n1_load.sum().sum(),
    n1_load.sum().max(),
    np.maximum(n1_load, 0).sum().mean()/np.maximum(n1_load, 0).sum().max(),
    -supply.u(n1.loc['mg.supply'].values, p=0),
    -delta_u,
    -supply.u(n1.loc['mg.supply'].values, p=0) - delta_u,
  ]
  df['DRF'] = [
    nmn_load.sum().sum(),
    nmn_load.sum().max(),
    np.maximum(nmn_load, 0).sum().mean()/np.maximum(nmn_load, 0).sum().max(),
    -supply.u(n1.loc['mg.supply'].values - balance.values, p=0),
    -delta_u,
    -supply.u(n1.loc['mg.supply'].values - balance.values, p=0) - delta_u,
  ]
  df2 = pd.DataFrame({}, index=index)
  df2['1 - DR/NODR']  = 1 - df['DR']/df['NODR']
  df2['1 - DRF/NODR'] = 1 - df['DRF']/df['NODR']
  df2['1 - DRF/DR']   = 1 - df['DRF']/df['DR']
  df2['NODR - DR']    = df['NODR'] - df['DR']
  df2['NODR - DRF']   = df['NODR'] - df['DRF']
  df2['DR - DRF']     = df['DR'] - df['DRF']
  print(df2)
  df.index = index_pres
  print(df.to_csv(float_format='%.3f', sep='&', line_terminator=' \\\\\n').replace('&', ' & ').replace('"', ''))


def table_load_by_type():
  ''' Total load by type break down. % only for space. Totals can be calculated easily from %. '''
  n0_load_by_type = n0_load.groupby(lambda row: re.sub('.*\.(\w+)$', '\\1', row)).sum().sum(axis=1)
  n1_load_by_type = n1_load.groupby(lambda row: re.sub('.*\.(\w+)$', '\\1', row)).sum().sum(axis=1)
  nm_load_by_type = nm_load.groupby(lambda row: re.sub('.*\.(\w+)$', '\\1', row)).sum().sum(axis=1)
  table = pd.DataFrame({}, index=n0_load_by_type.index)
  table['NO DR (%)'] = np.round(100*n0_load_by_type/n0_load_by_type.sum(), 1)
  table['DR (%)'] = np.round(100*n1_load_by_type/nm_load_by_type.sum(), 1)
  table['DRF (%)'] = np.round(100*nm_load_by_type/nm_load_by_type.sum(), 1)
  print(table.to_csv(float_format='%.3f', sep='&', line_terminator=' \\\\\n').replace('&', ' & '))
  table = pd.DataFrame({}, index=n0_load_by_type.index)
  table['NO DR (kWh)'] = np.round(n0_load_by_type, 3)
  table['DR (kWh)'] = np.round(n1_load_by_type, 3)
  table['DRF (kWh)'] = np.round(nm_load_by_type, 3)
  print(table.to_csv(float_format='%.3f', sep='&', line_terminator=' \\\\\n').replace('&', ' & '))



def table_fitting_error():
  ''' Aggregate error between relaxed profiles and refitted profiles in total and for all 4 discrete
  device types. MAPE is calculated as if the relaxed solution was the forecast to avoid division by zero.
  '''
  n1 = n1_load.groupby(lambda row: re.sub('.*\.(\w+)$', '\\1', row)).sum()
  nm = nm_load.groupby(lambda row: re.sub('.*\.(\w+)$', '\\1', row)).sum()
  n1.loc['total'] = n1.sum()
  nm.loc['total'] = nm.sum()
  table = pd.DataFrame({}, index=n1.index)
  table['Absolute (kW)'] = (n1 - nm).sum(axis=1)
  table['Relative (%)'] = 100*(n1 - nm).sum(axis=1)/n1.sum(axis=1)
  table['MAPE (%)'] = 100*((n1-nm)/n1).fillna(0).abs().sum(axis=1)/24
  print(table.to_csv(float_format='%.3f', sep='&', line_terminator=' \\\\\n').replace('&', ' & '))


def plot_gen_profile():
  supply = last.find_agents('supply')[0]
  plt.clf()
  plt.bar(range(24), -1*supply.r, label='generator')
  plt.plot(supply.device._cost_fn(supply.r*-1), 'r', label='price ($/kW)')
  plt.xlabel('Time (H)')
  plt.ylabel('Power (kW) or Price ($)')
  plt.legend(prop={'size': 12})
  plt.savefig('img/gen-profile.eps' )


def plot_pv_profile():
  pv = -1*first.find_agents('pv')[0].lbounds
  plt.clf()
  plt.bar(range(24), pv)
  plt.xlabel('Time (H)')
  plt.ylabel('Power (kW)')
  plt.savefig('img/pv-profile.eps' )


def plot_unsched_profile():
  n0_unsched = n0_load.loc[filter(lambda x: x.find('unsched') > 0, n0_load.index)].sort_index()
  bottoms = np.vstack((np.zeros((1, 24)), n0_unsched.cumsum(axis=0)))
  plt.clf()
  for i, v in enumerate(n0_unsched.iterrows()):
    key, row = v
    plt.bar(range(len(row)), row, bottom=bottoms[i], label='.'.join(key.split('.')[1:]))
  plt.xlabel('Time (H)')
  plt.ylabel('Power (kW)')
  plt.legend(prop={'size': 12},
    loc=(0.4,0.5),
    framealpha=1,
    frameon=True,
    fancybox=True,
    borderaxespad=-3
  )
  plt.savefig('img/unscheduled-profiles.eps')


def plot_base_case_profile():
  n1_base = pd.read_csv('%s-report/network-1.csv' % (d_base,), header=None, index_col=0)
  plt.clf()
  plt.bar(range(24), n1_base.loc['mg.load'], label='mapped')
  plt.xlabel('Time (H)')
  plt.ylabel('Power (kW)')
  plt.savefig('img/basecase-load-profile.eps' )


def plot_fitting_error_aggregate():
  print('Aggregate error for fitting:')
  n1 = n1_load.sum()
  nm = nm_load.sum()
  nm_tot_agg_error = (n1.sum() - nm.sum())
  nm_rel_agg_error = (n1.sum() - nm.sum())/nm.sum()
  plt.plot((n1-nm)/n1, label='model fit')
  plt.legend(prop={'size': 12})
  plt.grid(True, zorder=500)
  plt.show()


def scaling_of_dr():
  a = (n0_load - n1_load).sum(axis=1)
  b = n0_load.sum(axis=1)
  print('Relative scaling of DR by device', b/a)
  print('Relative scaling of DR', b.sum()/a.sum())


def battery_balancing_check():
  ''' Sanity check a thing about battery impact on demand '''
  battery = np.minimum(0, n1.loc['mg.battery'])
  n1_load.sum()
  print(n1_load.sum().sum() + battery.sum(), last.demand)


def plot_aggregate_fitting():
  fitting_plot(n1_load.sum(), nm_load.sum())
  plt.savefig('scenario2.py-network-fitting-report/fit-aggregate.eps')


def plot_fittings():
  samples = [
    'mg.site0.wpump',
    'mg.site1.wpump',
    'mg.site2.wpump',
    'mg.site3.wpump',
    'mg.site4.wpump',
    'mg.site5.wpump',
    'mg.site0.fridge',
    'mg.site1.fridge',
    'mg.site2.fridge',
    'mg.site3.fridge',
    'mg.site4.fridge',
    'mg.site5.fridge',
    'mg.site6.fridge',
    'mg.site7.fridge',
    'mg.site6.freezer',
    'mg.site7.freezer',
    'mg.site4.machine',
    'mg.site5.machine'
  ]
  for sample in samples:
    fitting_plot(n1.loc[sample], nm.loc[sample])
    plt.savefig('scenario2.py-network-fitting-report/fit-%s.eps' % (sample.replace('.', '-'),))
    plt.savefig('scenario2.py-network-fitting-report/fit-%s.png' % (sample.replace('.', '-'),))


def fitting_plot(r, f):
  plt.clf()
  ax = np.arange(24)
  plt.step(ax, r, 'g-', linestyle='-', markersize=3, label='relaxed')
  plt.step(ax, f, 'r-', linestyle='-', markersize=3, label='fitted')
  diff = (r - f)
  #plt.bar(ax-0.5, diff, 1.0, bottom=f, color='#AAAAAA')
  plt.plot(ax, diff, 'b_', linestyle='-', markersize=2, label='error')
  plt.legend(prop={'size': 12})
  plt.ylim(plt.ylim()[0], plt.ylim()[1]*1.1)
  plt.xlabel('Time (H)')
  plt.ylabel('Power (kW)')
  plt.grid(True, zorder=500)


def fitting_check():
  print(np.round(nm_load - n1_load, 2).sum(axis=1))


def plot_thermals():
  plt.clf()
  s = dict(last.leaf_items())
  fr_devices = {k: v for k,v in dict(last.device.leaf_devices()).items() if k.endswith('fridge') or k.endswith('freezer')}
  for k, d in fr_devices.items():
    plt.plot(d.r2t(s[k]), label=k)
    plt.legend(prop={'size': 12})
    plt.xlabel('Time (H)')
    plt.ylabel('Temperature')
    plt.savefig('img/temp-%s.eps' % (k.replace('.', '-'),))
    plt.clf()

@contextlib.contextmanager
def printoptions(*args, **kwargs):
    original = np.get_printoptions()
    np.set_printoptions(*args, **kwargs)
    try:
        yield
    finally:
        np.set_printoptions(**original)


if __name__ == '__main__':
  main()
