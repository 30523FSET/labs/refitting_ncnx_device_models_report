strict digraph {
  layout=circo
  node [style=filled,fontsize=38,fontname="Arial-Bold",fillcolor=green,shape=circle,width=3]
  edge [penwidth=3,arrowsize=3]
  # Layer 1
  LO   -> L11  [dir=both]
  LO   -> L12  [dir=both]
  DERG -> LO
  LO   -> DERS [dir=both]
  DERC -> LO   [dir=both]
  LO   -> L13  [dir=both]
  # Layer 2 House 1 ###
  L11 -> L11B [dir=both]
  L11 -> L11V # [dir=both]
  L11P -> L11
  L11 -> L11W
  L11 -> L11A
  # Layer 2 House 2 ###
  L12 -> L12B [dir=both]
  L12 -> L12V # [dir=both]
  L12P -> L12
  L12 -> L12W
  L12 -> L12A
  # Layer 2 House 3 ###
  L13P -> L13
  L13 -> L13W
  L13 -> L13A
  # Styles ###
  LO [style=filled,fillcolor=red,label="Microgrid Network",width=5]
  DERG [style=filled,fillcolor=orange,label="PV",width=3.5]
  DERS [style=filled,fillcolor=yellow,label="Battery",width=3.5]
  DERC [style=filled,fillcolor=red,label="Grid",width=3.5]
  L11 [style=filled,fillcolor=magenta,label="Sub-network",width=3.5]
  L12 [style=filled,fillcolor=magenta,label="Sub-network",width=3.5]
  L13 [style=filled,fillcolor=magenta,label="Sub-network",width=3.5]
  L11B [label="Battery",fillcolor=yellow]
  L12B [label="Battery",fillcolor=yellow]
  L11V [label="Appliance"]
  L12V [label="Appliance"]
  L11P [label="PV",fillcolor=orange]
  L12P [label="PV",fillcolor=orange]
  L13P [label="PV",fillcolor=orange]
  L11W [label="Appliance"]
  L12W [label="Appliance"]
  L13W [label="Appliance"]
  L11A [label="Appliance"]
  L12A [label="Appliance"]
  L13A [label="Appliance"]
}
