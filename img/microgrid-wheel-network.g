strict digraph {
		a -> s [dir=both, label="energy-flow"]
		b -> s [dir=both]
		c -> s [dir=both]
		d -> s [dir=both]
		e -> s [dir=both]
		f -> s [dir=both]
		a,b,c,d,e,f [label="Node", style=filled, fillcolor=gray];
		s [label="Network", style=filled, fillcolor=gray];
		splines="line";
		layout=circo;
	}
