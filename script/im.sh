#!/bin/bash
offset=4
for i in {0..7}; do
cat <<EOF
  <tr>
    <td>
      <figure id='f$(($i+$offset))'>
        <img style="max-width: 400px" src="./scenario2.py-network-latest-report/animation-site${i}-0000.png">
        <figcaption><strong>Figure $(($i+$offset)): Site ${i} no DR</strong></figcaption>
      </figure>
    </td>
    <td>
      <figure id='f$(($i+$offset+1))'>
        <img style="max-width: 400px" src="./scenario2.py-network-latest-report/animation-site${i}-0001.png">
        <figcaption><strong>Figure $(($i+$offset+1)): Site ${i} DR</strong></figcaption>
      </figure>
    </td>
  </tr>
EOF
done
