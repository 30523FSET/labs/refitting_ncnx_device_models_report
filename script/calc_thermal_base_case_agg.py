import numpy as np
import pandas as pd
from scenario2 import *
from powermarket.run import load_network


load_network('scenario2.py')
n = load_network('scenario2.py')[0]
n.init()
df = pd.DataFrame.from_dict(dict(n.leaf_items()), orient='index')
x = df.loc[filter(lambda x: x.find('fr') > 0, df.index)]
x.sort_index(axis=0, inplace=True)
y = pd.Series(np.array([0.12,0.12,0.12,0.12,0.12,0.12,0.25,0.12,0.25,0.12])*24, index=x.index)
print('Total difference')
print(x.sum(axis=1) - y)
