import numpy as np


fit = [2,11.25]
gens = [
  {'bounds': [10,50], 'cost': [0.006, 0.50, 0]},
  {'bounds': [8,45], 'cost': [0.003, 0.25, 0]},
  {'bounds': [15,70], 'cost': [0.004, 0.3, 0]},
]


def main():
  for g in gens:
    cg = np.poly1d(g['cost'])
    ax = np.linspace(g['bounds'][0], g['bounds'][1], 100)
    print(np.round(np.polyfit(ax*scale(g['bounds']), np.vectorize(cg)(ax)*scale(g['bounds']), deg=2), 3))

def scale(x):
  global fit
  return (fit[0]/x[0] + fit[1]/x[1])/2


if __name__ == '__main__':
  main()
