#!/bin/bash
offset=21
img=(mg.site0.wpump mg.site1.wpump mg.site0.fridge mg.site1.fridge mg.site6.freezer mg.site7.freezer mg.site4.machine mg.site5.machine)
for i in ${img[@]}; do
offset=$(($offset+1))
cat <<EOF
  <tr>
    <td>
      <figure id='f${offset}'>
        <img style="max-width: 400px" src="./scenario2.py-network-latest-fitting-report/fit-$i.png">
        <figcaption><strong>Figure ${offset}: Site ${i} no DR</strong></figcaption>
      </figure>
    </td>
    <td>
      <figure id='f${offset}'>
        <img style="max-width: 400px" src="./scenario2.py-network-latest-fitting-report/fit-naive-$i.png">
        <figcaption><strong>Figure ${offset}: Site ${i} DR</strong></figcaption>
      </figure>
    </td>
  </tr>
EOF
done
