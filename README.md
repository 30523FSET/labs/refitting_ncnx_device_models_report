# Overview
This repository contains a microgrid day-ahead scheduling [scenario](scenario2.py) written for the [powermarket](/30523FSET/powermarket) / [device_kit](/30523FSET/device_kit) tools. It also contains result data and an accompanying [PDF report](report/report.pdf).

The main objective of this experiment is to investigate the feasibility of relaxation and remapping approach to to centralized optimization of almost convex models with some simple types of non convex constraints. Please refer to the report for further info.

# Scenario Overview ##################################################
This case study evaluates the modeling framework and also the effect of the relaxation approach described above, in a hypothetical small islanded microgrid. A comparative analysis is performed between three configurations of the same microgrid scenario:

  - No DR base case.
  - Relaxed infeasible case.
  - Relaxed case with discrete constraints re-introduced and refitted.

The make up the microgrid is based on future projections for real life remote rural microgrid case study. There are 8 rural sites connected to the MG. The MG has central:

  - Thermal fuel based generator.
  - PV installation.
  - Large chemical battery storage.

Each site contains a community of people living and working at and around the site. All sites share the same base set of devices and two site types extend that base set.

The exact parameters to the scenario are soemtimes updated so refer to [scenario.py](scenario2.py) for the single source of truth.

# Appendix ##################################################

## Analysing Results
There is two outcomes we want to evaluate:

  1. NODR vs DR cases.
  2. The re-fitting of relaxed constraints to the DR case solution.

The key results we want to show:

  - A comparison of the NO DR vs DR. The DR case sets the upper bound on potential of DR. Basic summary stats (peak, load factor, total consumption, costs), and some load profiles.
  - The effect of the mapping: How bad is the fit overall? What is the effect on costs?

The effect on costs depends on how things are priced. There are two obvious altrnatives 1. Assume the "owner" of the DER is the tenants and the tenants are cooperative. In this case there is no profit. Price is C(Q)/Q + e where e is some small +ve number to cover operations etc. This is ultimately unrealistic and troublesom

## Work flow
Notes on how data is generated:

    . dev.sh
    # Gen the base-case data:
    ./powermarket/solve.py scenario2_base_case.py --ftol=0.005 -d scenario2.py-network-latest-base-case
    ./powermarket/solve.py scenario2.py --ftol=0.005 -d scenario2.py-network-latest
    ./powermarket/analytics/report.py --movie --movies scenario2.py-network-latest/
    ./powermarket/analytics/report.py --movie --movies scenario2.py-network-latest-base-case/
    # Checkout the data make sure it is what you want ot report on. If not repeat.
    # The second part is generating the fittings. Use MIQP via SCIP library:
    # fit.py will filter a CSV file; all rows that it detects have discrete constraints will be mapped. The rest stay the same:
    ./discrete_device_models_pyscipopt/fit.py ./scenario2.py-network-latest-report/network-1.csv
    # network-1.csv is the result of convex optimization. This script has a implicit dep no scenario2.py which it loads to figure out which devices have non convex constraints then build the discrete models for.
    ./report.py > report.txt
    # Check results.
    cat report.txt
    display scenario2.py-network-fitting-report/*.eps
