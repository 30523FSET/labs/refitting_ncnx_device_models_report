#!/usr/bin/env python3
'''
This script maps relaxed profiles from an input CSV file to discrete profiles for each site that
has a discrete models.

Relaxed profiles are provided via a CSV file. Output is CSV file matching shape, with discrete
constraints mapped on to the devices in each row.
'''
import sys
import re
import logging
import argparse
import numpy as np
import pandas as pd
from copy import deepcopy
import pyscipopt
from pyscipopt import Model, quicksum, multidict
from mapping_models.scenario2 import make_network


logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def main():
  ''' Read pre generated results CSV file, and read network model from scenario2. For each discrete
  device found look for it's result row in results and find the feasible profile with min SSE from
  the result. Dump results back to a CSV.
  '''
  parser = argparse.ArgumentParser(description='Mao discrete models.')
  parser.add_argument('reference_results_file', type=argparse.FileType('r'),
    help='reference results CSV file'
  )
  args = parser.parse_args()

  network = make_network()
  results = pd.read_csv(args.reference_results_file, header=None, index_col=0, names=range(24))
  mapped_results = results.copy()
  devices = dict(network.device.leaf_devices())

  ''' For each site build a model (in fact many models) find min SSE then update rows in mapped_results '''
  for site_key in ['mg.site%d' % (i,) for i in range(8)]:
    logging.debug(site_key)
    rows = results.loc[filter(lambda x: re.match('%s\.(wpump|machine)$' % (site_key,), x), results.index)]
    for k, v in rows.iterrows():
      rows.loc[k] /= devices[k].step
    print(rows)
    models = []
    wpump_key = '%s.%s' % (site_key, 'wpump')
    models = [sum_model(Model(), wpump_key, DeviceModel(devices[wpump_key]))]
    try:
      machine_key = '%s.%s' % (site_key, 'machine')
      models = uninteruptable_shiftable_models(models[0], machine_key, DeviceModel(devices[machine_key]))
    except KeyError:
      pass
    best = None
    best_cost = 1e10

    for model in models:
      s = get_vars(model, rows)
      print(s)
      o = model.addVar(vtype="CONTINUOUS", name="SSE", lb=0, ub=24*4**2)
      model.addCons(quicksum([(quicksum(s[i].values) - quicksum(rows[i].values))**2 for i in s]) == o)
      model.setObjective(o, "minimize")
      model.optimize()
      if model.getObjVal() < best_cost:
        best_cost = model.getObjVal()
        best = s.applymap(lambda x: model.getVal(x))+0
        for k, v in best.iterrows():
          best.loc[k] *= devices[k].step
        print(best)
        best = best
    for k in best.index:
      mapped_results.loc[k] = best.loc[k]

  print(mapped_results)

  ext = '.mapped.csv'
  filename = re.sub('\.csv$', ext, args.reference_results_file.name)
  logging.info('Output to %s' % filename)
  mapped_results.to_csv(
    filename,
    header=None,
    float_format='%0.5f'
  )


def get_vars(model, rows):
  ''' Get the bloody variables back out! '''
  variables = pd.DataFrame(rows, dtype=object)
  for v in model.getVars():
    r, i = re.match('(mg\.site.?\.\w+)\.(\d+)$', v.name).groups()
    variables.loc[r, int(i)] = v
  return variables


class DeviceModel():
  ''' Wrapper to create a discrete PySCIPOpt model from powermarket.device_kit device model.
  '''

  def __init__(self, device):
    ''' Collect all discrete vars. Only cbounds and bounds are stepped up, not levels. '''
    assert len(device.on) % 2 == 0
    self.device = device
    self.levels = tuple(device.levels)
    self.cbounds = (int(device.cbounds[0]/device.step), int(device.cbounds[1]/device.step))
    self.on = tuple(device.on)
    self.run_time = tuple(device.run_time) if hasattr(device, 'run_time') else None

  def __len__(self):
    return len(self.device)

  def __str__(self):
    return '%s-%d-%d-%d-%d-%d-%d' % (
      self.model,
      len(self),
      self.on[0],
      self.on[1],
      self.levels[1],
      self.cbounds[0],
      self.cbounds[1]
    )


def sum_model(model, key, device):
  ''' Create a model for a single device's discrete states.
    - `levels` is an array with even length. [0,3] means the devices has run modes 0,1,2,3.
      [0,2,6,6] means 0,1,2,6. Levels must be integers.
    - `cbounds` is a two tuple cummulative lower and upper bound.
    - `on` is an array of integers specifying which variables the device can be on, with the same syntax as `levels`.
  '''
  variables = []
  if len(device.levels) > 2:
    raise Exception('non contiguous levels not currently supported')
  for i in range(len(device)):
    if i in list(ons(device.on)):
      variables.append(model.addVar(vtype='INTEGER', name='%s.%d' % (key, i,), lb=device.levels[0], ub=device.levels[1]))
    else:
      variables.append(model.addVar(vtype='INTEGER', name='%s.%d' % (key, i,), lb=0, ub=0))
  model.addCons(device.cbounds[0] <= quicksum(variables))
  model.addCons(device.cbounds[1] >= quicksum(variables))
  return model


def integral_model(model, key, device):
  ''' This is currently nothing special. It is just the particular model used for thermal devices
  which is just a sum_model.
  '''
  return sum_model(model, key, device)


def uninteruptable_shiftable_models(model, key, device, allow_zero_level=False):
  ''' Shiftability is represented by the union of N models one for each possible shift. Within each
  shift a simple sum constraint is applied. Unfortunately AFAIK I can't OR two or more models
  together. More over a var belongs to exactly one model. So return a list of models, vars named
  the same (via their index) in each model are semantically the same var.
  '''
  models = []
  if len(device.on) != 2:
    ValueError('Size of `on` must be two - only contiguous range is support [%s]' % (str(on),))
  for run in schedules(device.on, device.run_time):
    _device = deepcopy(device)
    _device.on = run
    if not allow_zero_level and 0 in _device.levels:
      _device.levels = [1 if v == 0 else v for v in _device.levels]
    models.append(sum_model(Model(sourceModel=model), key, _device))
  return models


class schedules():
  ''' Helper to generate valid uninteruptable run time schedules anywhere in `on` of length
   `run_time`
  '''

  def __init__(self, on, run_time):
    self.on, self.run_time = on, (run_time[0]-1, run_time[1])

  def __iter__(self):
    for size in range(*self.run_time):
      start = self.on[0]
      while start + size < self.on[1] + 1:
        yield (start, start+size)
        start += 1


def ons(on):
  ''' Helper function. '''
  for i in range(0, len(on), 2):
    for j in range(on[i], on[i+1]+1):
      yield j


def filter_row(k, v, device):
  ''' If discrete model exist for SSE device map it, otherwise just return input value `v`. '''
  model = make_device_model(device)
  if model:
    value = model.min_sse(v/device.step)
    logging.debug(np.round(v, 2))
    logging.debug(np.round(value*device.step, 2))
    return value*device.step
  return v


if  __name__ == '__main__':
  main()
