#!/usr/bin/env python3
'''
This script maps relaxed profiles from an input CSV file to discrete profiles for each device that
has a discrete model.

Relaxed profiles are provided via a CSV file. Output is CSV file matching shape, with discrete
constraints mapped on to the devices in each row.
'''
import sys
import re
import logging
import argparse
import numpy as np
import pandas as pd
from copy import deepcopy
from powermarket.network import Network
from refitting_ncnx_device_models_report.scenario2 import make_deviceset
from pyscipopt import Model, quicksum, multidict

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def main():
  ''' Read pre generated results CSV file, and read network model from scenario2. For each discrete
  device found look for it's result row in results and find the feasible profile with min SSE from
  the result. Dump results back to a CSV.
  '''
  parser = argparse.ArgumentParser(description='Mao discrete models.')
  parser.add_argument('reference_results_file', type=argparse.FileType('r'),
    help='reference results CSV file'
  )
  args = parser.parse_args()

  network = Network.from_deviceset(make_deviceset())
  results = pd.read_csv(args.reference_results_file, header=None, index_col=0)
  mapped_results = results.copy()
  devices = dict(network.device.leaf_devices())

  for k, v in results.iterrows():
    device = devices[k]
    mapped_results.loc[k] = filter_row(k, v.values, device)

  print(mapped_results)

  ext = '.mapped.csv'
  filename = re.sub('\.csv$', ext, args.reference_results_file.name)
  logging.info('Output to %s' % filename)
  mapped_results.to_csv(
    filename,
    header=None,
    float_format='%0.5f'
  )


def filter_row(k, v, device):
  ''' If discrete model exist for SSE device map it, otherwise just return input value `v`. '''
  model = make_device_model(device)
  if model:
    value = model.min_sse(v/device.step)
    logging.debug(np.round(v, 2))
    logging.debug(np.round(value*device.step, 2))
    return value*device.step
  return v


def make_device_model(device):
  model = None
  if hasattr(device, 'run_time') and not device.__class__.__name__ == 'GDevice':
    model = UninteruptableShiftableDeviceModel(device)
    logging.info('Found uninteruptable shiftable device %s [%s]' % (device.id, model))
  elif hasattr(device, 'levels') and device.__class__.__name__ == 'TDevice':
    model = IntegralDeviceModel(device)
    logging.info('Found discrete thermal device %s [%s]' % (device.id, model))
  elif hasattr(device, 'levels'):
    model = SumDeviceModel(device)
    logging.info('Found discrete device %s [%s]' % (device.id, model))
  return model


class DeviceModel():
  ''' Wrapper to create a discrete PySCIPOpt model from powermarket.device_kit device model.
  '''

  def __init__(self, device):
    ''' Collect all discrete vars. Only cbounds and bounds are stepped up, not levels. '''
    assert len(device.on) % 2 == 0
    self.device = device
    self.levels = tuple(device.levels)
    self.cbounds = (int(device.cbounds[0]/device.step), int(device.cbounds[1]/device.step))
    self.on = tuple(device.on)
    self.run_time = tuple(device.run_time) if hasattr(device, 'run_time') else None

  def __len__(self):
    return len(self.device)


  def __str__(self):
    return '%s-%d-%d-%d-%d-%d-%d' % (
      self.model,
      len(self),
      self.on[0],
      self.on[1],
      self.levels[1],
      self.cbounds[0],
      self.cbounds[1]
    )

  def min_sse(self, val):
    o = self.model.addVar(vtype="CONTINUOUS", name="SSE", lb=0, ub=len(self)*self.levels[-1]**2)
    self.model.addCons(quicksum([(self.variables[i] - val[i])**2 for i in range(len(self))]) == o)
    self.model.setObjective(o, "minimize")
    self.model.optimize()
    argmin = np.array([self.model.getVal(v) for v in self.variables])
    return argmin+0


class SumDeviceModel(DeviceModel):

  def __init__(self, device):
    super(SumDeviceModel, self).__init__(device)
    self.model, self.variables = sum_model(len(self), self.levels, self.cbounds, self.on)


class IntegralDeviceModel(DeviceModel):

  def __init__(self, device):
    super(IntegralDeviceModel, self).__init__(device)
    self.model, self.variables = integral_model(len(self), self.levels, self.cbounds, self.on)


class UninteruptableShiftableDeviceModel(DeviceModel):

  def __init__(self, device):
    super(UninteruptableShiftableDeviceModel, self).__init__(device)
    self.model = uninteruptable_shiftable_models(len(self), self.levels, self.cbounds, self.on, self.run_time)

  def min_sse(self, val):
    best = None
    best_cost = 1e10
    for model, variables in self.model:
      o = model.addVar(vtype="CONTINUOUS", name="SSE", lb=0, ub=len(self)*self.levels[-1]**2)
      model.addCons(quicksum([(variables[i] - val[i])**2 for i in range(len(self))]) == o)
      model.setObjective(o, "minimize")
      model.optimize()
      if model.getObjVal() < best_cost:
        best_cost = model.getObjVal()
        best = np.array([model.getVal(v) for v in variables])
    return best


def sum_model(length, levels, cbounds, on):
  ''' Create a model for a single device's discrete states.
    - `levels` is an array with even length. [0,3] means the devices has run modes 0,1,2,3.
      [0,2,6,6] means 0,1,2,6. Levels must be integers.
    - `cbounds` is a two tuple cummulative lower and upper bound.
    - `on` is an array of integers specifying which variables the device can be on, with the same syntax as `levels`.
  '''
  model = Model()
  variables = []
  if len(levels) > 2:
    raise Exception('non contiguous levels not currently supported')
  for i in range(length):
    if i in list(ons(on)):
      variables.append(model.addVar(vtype='INTEGER', name='%d' % (i,), lb=levels[0], ub=levels[1]))
    else:
      variables.append(model.addVar(vtype='INTEGER', name='%d' % (i,), lb=0, ub=0))
  model.addCons(cbounds[0] <= quicksum(variables))
  model.addCons(cbounds[1] >= quicksum(variables))
  return model, variables


def integral_model(length, levels, cbounds, on, nominal=1, drift=1):
  ''' This is currently nothing special. It is just the particular model used for thermal devices
  which is just a sum_model.
  '''
  model, variables = sum_model(length, levels, cbounds, on)
  #for i in range(len(variables)-1):
  #  model.addCons(2*nominal - drift <= variables[i] + variables[i+1])
  #  model.addCons(2*nominal + drift >= variables[i] + variables[i+1])
  return model, variables


def uninteruptable_shiftable_models(length, levels, cbounds, on, run_time, allow_zero_level=False):
  ''' Shiftability is represented by the union of N models one for each possible shift. Within each
  shift a simple sum constraint is applied. Unfortunately AFAIK I can't OR two or more models
  together. More over a var belongs to exactly one model. So return a list of models, vars named
  the same (via their index) in each model are semantically the same var.
  '''
  models = []
  if len(on) != 2:
    ValueError('Size of `on` must be two - only contiguous range is support [%s]' % (str(on),))
  for run in schedules(on, run_time):
    if not allow_zero_level and 0 in levels:
      levels = [1 if v == 0 else v for v in levels]
    models.append(sum_model(length, levels, cbounds, run))
  return models


class schedules():
  ''' Helper to generate valid uninteruptable run time schedules anywhere in `on` of length
   `run_time`
  '''

  def __init__(self, on, run_time):
    self.on, self.run_time = on, (run_time[0]-1, run_time[1])

  def __iter__(self):
    for size in range(*self.run_time):
      start = self.on[0]
      while start + size < self.on[1] + 1:
        yield (start, start+size)
        start += 1


def ons(on):
  ''' Helper function. '''
  for i in range(0, len(on), 2):
    for j in range(on[i], on[i+1]+1):
      yield j


if  __name__ == '__main__':
  main()
