## Note on TDevice Setup

### Initial Solution and Cumulative Bound Issue ###
Another issue is; suppose under zero cost solution thermal device is hitting exact set point. In unconstrained optimal result consumption will be lower because temperature is traded off against cost. By imposing cumulative constraint we are prevent such a long run tradeoff. This is OK. But initially it is hard to set parameters to hit exact consumption: to fix this, ensure under under zero cost solution thermal device is consuming just above the set point. To allow this just set cbounds to [24, 24+1]*step. See script/calc_thermal_base_case_agg.py
