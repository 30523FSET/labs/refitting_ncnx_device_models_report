#!/bin/bash -e
./powermarket/solve.py scenario2_base_case.py --ftol=0.01 -d scenario2.py-network-latest-base-case
./powermarket/solve.py scenario2.py --ftol=0.005 -d scenario2.py-network-latest
./powermarket/analytics/report.py --movie --movies scenario2.py-network-latest/
./powermarket/analytics/report.py --movie --movies scenario2.py-network-latest-base-case/
./discrete_device_models_pyscipopt/fit.py ./scenario2.py-network-latest-report/network-1.csv
./report.py > report.txt
