#!/bin/bash -e
[[ -z "$1" ]] && echo "Usage: $0 <file>.tex" && exit 1;
pdflatex "$1"
bibtex "$1"
pdflatex "$1"
pdflatex "$1"
#./clean.sh "$1"
