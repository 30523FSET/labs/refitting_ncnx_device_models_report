% Claessen2014 has uninterruptible shiftable but modelling is less only vaguely alluded to not defined formerly.

%%% I wanted to add something like this to add to motivation, but it's ... hard to explain ... and getting philosophical.
% It is technically true that convex modelling can not capture many constraints with complete precision. But all model are wrong, or rather right to varying degrees of precision. The question is one of trade-offs and fitness for the given purpose. There are a number of important factors to consider. Firstly day-ahead scheduling in microgrids are based on demand and supply forecast typically have a degree of uncertainty. The solution is inevitably wrong and needs to be adjusted in realtime. Secondly, mathematical models form the basis of real life systems. They become design metaphors. It is not a matter of picking and choosing modelling frameworks and related optimization techniques on a case by case basis. This won't scale. Third, as X points out the distinction between prescriptive and descriptive modelling needs to be appreciated. We model weather is a descriptive way because we have no (or very little) control over it. Microgrids and their controllable devices are not naturally occurring phenomena. Within reason, it is legitimate to change the subject to match the modelling framework if it adds up.




%\begin{align}
%  window\_dispersion\_cost(Q_d) = k\sum_{t \in T} Q_{d,t} (t - \bar{t})^2
%\end{align}
%
% where $\bar{t}$ is the $Q_d$ weighted average time interval, and $k$ is some constant weighting factor. This cost function penalises solutions in which $Q_d$ is spread out, thus not fitting well within a given window, while not giving preference to any particular window, making the relaxed solution closer to a feasible one when the window constraint is reintroduced. Admittedly, it is an open question what weighting factor $k$ to use. The weight should be somehow based on the balancing cost due to refitting a very bad relaxed solution.

%\subsubsection{Balancing Cost of Refitting}
%Clearly, the refitting results in an error between the relaxed scheduling solution and actual schedule of devices. Prices and corresponding production and consumption commitments will be made based on the relaxed solution. The resulting error can be interpreted and dealt with in a similar manner as a forecasting error. The necessary adjustments of the schedules due to forecasting errors is handled by some sort of real time, or near real time balancing. Very different approaches to the pricing of balancing adjustments are possible. In this work, we assume

%It should be noted, an obvious difference between the fitting error and forecasting error is that the former is known in advance, and can therefore potentially be dealt with in advance by some kind of secondary coordination process. While interesting, we don't explore that point further in this work. Treating the fitting error like a forecasting error represents a worst case scenario.
